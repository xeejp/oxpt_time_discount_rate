import React, { useReducer, useEffect, useState } from 'react'
import { useStore } from './actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import i18nInstance from './i18n'

const useStyles = makeStyles(theme => ({
  root: {
    padding: 0,
    height: '100vh',
    overflow: 'auto',
    margin: theme.spacing(1)
  },
  contents: {
    width: 300,
    margin: 'auto'
  },
  formControl: {
    margin: theme.spacing(3),
  },
  group: {
    margin: theme.spacing(1, 0),
  },
}))

export default () => {
  const classes = useStyles()
  const { locales, players, page, pushState } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [_, forceUpdate] = useReducer(x => x + 1, 0)

  const onClickPage = page => {
    pushState({ event: 'change page', payload: page })
  }

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
    console.log("changed locales")
  }, [locales])

  if (!(locales))
    return <></>

  return (
    <></>
  )
}
