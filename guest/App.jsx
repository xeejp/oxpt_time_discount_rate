import React, { useReducer, useEffect, useState } from 'react'
import { useStore } from './actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'

import i18nInstance from './i18n'

const useStyles = makeStyles(theme => ({
  items: {
    padding: theme.spacing(2)
  }
}))

export default () => {
  const classes = useStyles()
  const { page, locales } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [_, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(page && locales))
    return <></>

  return (
    <></>
  )
}
