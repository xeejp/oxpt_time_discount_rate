defmodule Oxpt.TimeDiscountRate.Guest do
  @moduledoc """
  Documentation for Oxpt.TimeDiscountRate.Guest
  """

  use Cizen.Automaton
  defstruct [:room_id]

  use Cizen.Effects
  use Cizen.Effectful
  alias Cizen.{Event, Filter}
  alias Oxpt.JoinGame

  alias Oxpt.TimeDiscountRate.{Host, Locales}

  alias Oxpt.TimeDiscountRate.Events.{
    FetchState,
    UpdateState,
    UpdateStateAll,
    ChangePage,
    JoinGuest,
    UpdateGuest,
    UpdateHost,
    ChangeAlloTemp,
    FinishAllocating,
    Response,
    ResetResponse,
    RedoAllocating
  }

  use Oxpt.Game,
    root_dir: Path.join(__ENV__.file, "../../guest") |> Path.expand()

  @impl Oxpt.Game
  def player_socket(game_id, %__MODULE__{room_id: _room_id}, guest_id) do
    %__MODULE__.PlayerSocket{game_id: game_id, guest_id: guest_id}
  end

  @impl Oxpt.Game
  def new(room_id, _params) do
    %__MODULE__{room_id: room_id}
  end

  @impl true
  def spawn(id, %__MODULE__{room_id: room_id}) do
    host_game_id = perform(id, %Start{saga: Host.new(room_id, game_id: id)})

    perform(id, %Subscribe{
      event_filter: Filter.new(fn %Event{body: %JoinGame{game_id: ^id}} -> true end)
    })

    perform(id, %Subscribe{
      event_filter:
        Filter.new(fn
          %Event{
            body: %FetchState{game_id: ^id}
          } ->
            true

          %Event{
            body: %ChangePage{game_id: ^id}
          } ->
            true

          %Event{
            body: %UpdateGuest{game_id: ^id}
          } ->
            true

          %Event{
            body: %ChangeAlloTemp{game_id: ^id}
          } ->
            true

          %Event{
            body: %FinishAllocating{game_id: ^id}
          } ->
            true

          %Event{
            body: %Response{game_id: ^id}
          } ->
            true

          %Event{
            body: %ResetResponse{game_id: ^id}
          } ->
            true

          %Event{
            body: %RedoAllocating{game_id: ^id}
          } ->
            true
        end)
    })

    initial_state = %{
      host: host_game_id,
      page: "instruction",
      players: %{},
      groups: %{},
      locales: Locales.get(),
      game_rounds: 1,
      game_redo: 0,
      inf_redo: false,
      initial_amount: 1000,
      time_discount_rate_results: %{}
    }

    {:loop, initial_state}
  end

  @impl true
  def yield(id, {:loop, state}) do
    event = perform(id, %Receive{})

    state = handle_event_body(id, event.body, state)

    {:loop, state}
  end

  defp state_filter(state) do
    %{
      page: state.page,
    #  locales: state.locales, debug
      game_rounds: state.game_rounds,
      game_redo: state.game_redo,
      inf_redo: state.inf_redo,
      initial_amount: state.initial_amount
    } |> Map.merge(
      if state.page == "result" do
        %{
          time_discount_rate_results: state.time_discount_rate_results
        }
      else
        %{}
      end
    )
  end

  defp handle_event_body(id, %JoinGame{guest_id: guest_id, host: true}, state) do
    perform(id, %Request{
      body: %JoinGame{game_id: state.host, guest_id: guest_id, host: true}
    })

    state
  end

  defp handle_event_body(id, %JoinGame{guest_id: guest_id}, state) do
    player = new_player(state)
    new_state = put_in(state, [:players, guest_id], player)

    perform(id, %Dispatch{
      body: %JoinGuest{
        game_id: state.host,
        payload: %{
          player: player,
          guest_id: guest_id
        }
      }
    })

    new_state
  end

  defp handle_event_body(id, %FetchState{guest_id: guest_id}, state) do
    player = state.players[guest_id]

    group =
      if player.group_id != nil && Map.size(state.groups) != 0 do
        state.groups[player.group_id]
      else
        nil
      end

    perform(id, %Dispatch{
      body: %UpdateState{
        guest_id: guest_id,
        state:
          player
          |> Map.merge(%{
            # debug
            locales: Locales.get(),
            group: group
          })
          |> Map.merge(state_filter(state))
      }
    })

    state
  end

  defp handle_event_body(id, %UpdateGuest{state: host_state}, state) do
    new_state =
      state
      |> Map.merge(%{
        page: host_state.page,
        locales: host_state.locales,
        players: host_state.players,
        groups: host_state.groups,
        game_rounds: host_state.game_rounds,
        game_redo: host_state.game_redo,
        inf_redo: host_state.inf_redo,
        initial_amount: host_state.initial_amount,
        time_discount_rate_results: host_state.time_discount_rate_results
      })

    Enum.each(Map.keys(host_state.players), fn guest_id ->
      player = host_state.players[guest_id]
      group = host_state.groups[player.group_id]

      perform(id, %Dispatch{
        body: %UpdateState{
          guest_id: guest_id,
          state:
            player
            |> Map.merge(%{
              group: group
            })
            |> Map.merge(state_filter(new_state))
        }
      })
    end)

    new_state
  end

  defp handle_event_body(id, %ChangePage{page: page}, state) do
    new_state =
      state
      |> Map.put(:page, page)

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          page: new_state.page
        }
      }
    })

    new_state
  end

  defp handle_event_body(id, %ChangeAlloTemp{guest_id: guest_id, allo_temp: allo_temp}, state) do
    group_id = state.players[guest_id].group_id
    group = state.groups[group_id]

    "allocating" = group.game_state
    new_state = put_in(state, [:groups, group_id, :allo_temp], allo_temp)

    group_update(id, group.members, new_state, new_state.groups[group_id])

    new_state
  end

  defp handle_event_body(id, %FinishAllocating{guest_id: guest_id}, state) do
    group_id = get_in(state, [:players, guest_id, :group_id])

    new_state = put_in(state, [:groups, group_id, :game_state], "judging")
    new_group = group = get_in(new_state, [:groups, group_id])

    group_update(id, group.members, new_state, new_group)

    new_state
  end

  defp handle_event_body(id, %Response{guest_id: guest_id, accept: accept}, state) do
    group_id = get_in(state, [:players, guest_id, :group_id])
    group = get_in(state, [:groups, group_id])
    game_rounds = state.game_rounds
    initial_amount = state.initial_amount

    allo = group.allo_temp
    now_round = group.now_round
    members = group.members

    target_id =
      case members do
        [^guest_id, target_id] -> target_id
        [target_id, ^guest_id] -> target_id
      end

    guest = get_in(state, [:players, guest_id])
    target = get_in(state, [:players, target_id])
    guest_role = guest.role
    target_role = target.role

    "judging" = group.game_state

    new_state =
      state
      |> put_in([:players, guest_id, :role], get_next_role(guest_role))
      |> put_in([:players, target_id, :role], get_next_role(target_role))
      |> put_in([:groups, group_id, :redo_count], 0)
      |> put_in(
        [:groups, group_id, :game_state],
        case now_round < game_rounds do
          true -> "allocating"
          false -> "finished"
        end
      )
      |> put_in(
        [:groups, group_id, :now_round],
        case now_round < game_rounds do
          true -> now_round + 1
          false -> now_round
        end
      )
      |> put_in([:groups, group_id, :allo_temp], div(initial_amount,2))
      |> update_in([:groups, group_id, :group_results], fn list ->
        [%{proposer: target_id, allo: allo, accepted: accept} | list]
      end)
      |> put_in([:time_discount_rate_results], Map.merge(get_in(state, [:time_discount_rate_results]), %{
        Integer.to_string(now_round) => Map.merge(get_in(state, [:time_discount_rate_results, Integer.to_string(now_round)]) || %{}, %{
          group_id => %{
            allo: allo,
            accept: accept
          }
        })
      }))

    new_state =
      if accept do
        new_state
        |> update_in([:players, guest_id, :point], fn point ->
          case guest_role == "responder" do
            true -> point + (initial_amount - allo)
            false -> point + allo
          end
        end)
        |> update_in([:players, target_id, :point], fn point ->
          case target_role == "responder" do
            true -> point + (initial_amount - allo)
            false -> point + allo
          end
        end)
        |> put_in([:groups, group_id, :response], "OK")
      else
        new_state
        |> put_in([:groups, group_id, :response], "NG")
      end

    [id1, id2] = members
    new_player1 = get_in(new_state, [:players, id1])
    new_player2 = get_in(new_state, [:players, id2])
    new_group = get_in(new_state, [:groups, group_id])
    group_update(id, members, new_state, new_group, [new_player1, new_player2])

    if Enum.all?(Map.keys(new_state.groups), &(get_in(new_state, [:groups, &1, :game_state]) == "finished")) do
      perform(id, %Dispatch{
        body: %ChangePage{
          game_id: state.host,
          page: "result"
        }
      })
    end

    new_state
  end

  defp handle_event_body(id, %RedoAllocating{guest_id: guest_id}, state) do
    group_id = get_in(state, [:players, guest_id, :group_id])

    new_state =
      state
      |> update_in([:groups, group_id, :redo_count], &(&1 + 1))
      |> put_in([:groups, group_id, :game_state], "allocating")
      |> put_in([:groups, group_id, :response], "REDO")

    new_group = group = get_in(new_state, [:groups, group_id])

    group_update(id, group.members, new_state, new_group)

    new_state
  end

  defp handle_event_body(id, %ResetResponse{guest_id: guest_id}, state) do
    group_id = get_in(state, [:players, guest_id, :group_id])

    new_state = put_in(state, [:groups, group_id, :response], "")
    new_group = group = get_in(new_state, [:groups, group_id])

    group_update(id, group.members, new_state, new_group)

    new_state
  end

  defp group_update(id, members, state, group, player_data \\ [%{}, %{}]) do
    [id1, id2] = members
    [player1, player2] = player_data

    perform(id, %Dispatch{
      body: %UpdateState{
        guest_id: id1,
        state:
          player1
          |> Map.merge(%{
            group: group
          })
      }
    })

    perform(id, %Dispatch{
      body: %UpdateState{
        guest_id: id2,
        state:
          player2
          |> Map.merge(%{
            group: group
          })
      }
    })

    perform(id, %Dispatch{
      body: %UpdateHost{
        game_id: state.host,
        state: %{
          groups: state.groups,
          players: state.players,
          time_discount_rate_results: state.time_discount_rate_results
        }
      }
    })
  end

  defp get_next_role(role) do
    case role == "responder" do
      true -> "proposer"
      false -> "responder"
    end
  end

  defp new_player(_state) do
    %{
      role: nil,
      group_id: nil,
      point: 0
    }
  end
end
