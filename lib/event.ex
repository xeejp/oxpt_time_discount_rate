defmodule Oxpt.TimeDiscountRate.Events do
  @moduledoc """
  Documentation for Oxpt.TimeDiscountRate.Events

  gameで使うイベントを定義する
  """

  defmodule UpdateStateAll do
    @moduledoc """
    すべてのplayer_socketがsubscribeしておくべきイベント。
    全員に対しあるstateを送りたいときに使う

    """
    defstruct [:game_id, :state]
  end

  defmodule UpdateState do
    @moduledoc """
    guest_idで指定されたゲストのplayer_socketがsubscribeしておくべきイベント。
    そのゲストに対しあるstateを送りたいときに使う

    """
    defstruct [:guest_id, :state]
  end

  defmodule FetchState do
    @moduledoc """
    stateを管理しているオートマトンがsubscribeしておくべきイベント。
    player_socketがspawnした時にDispatchし、クライアントにstateを送る

    """
    defstruct [:game_id, :guest_id]
  end

  defmodule UpdateGuest do
    defstruct [:game_id, :state]
  end

  defmodule UpdateHost do
    defstruct [:game_id, :state]
  end

  defmodule ChangePage do
    defstruct [:game_id, :page]
  end

  defmodule ChangeSetting do
    defstruct [:game_id, :payload]
  end

  defmodule JoinGuest do
    defstruct [:game_id, :payload]
  end

  defmodule ChangeAlloTemp do
    defstruct [:game_id, :guest_id, :allo_temp]
  end

  defmodule FinishAllocating do
    defstruct [:game_id, :guest_id]
  end

  defmodule Response do
    defstruct [:game_id, :guest_id, :accept]
  end

  defmodule ResetResponse do
    defstruct [:game_id, :guest_id]
  end

  defmodule RedoAllocating do
    defstruct [:game_id, :guest_id]
  end
end
