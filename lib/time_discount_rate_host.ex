defmodule Oxpt.TimeDiscountRate.Host do
  @moduledoc """
  Documentation for Oxpt.TimeDiscountRate.Host
  """

  use Cizen.Automaton
  defstruct [:room_id, :guest_game_id]

  use Cizen.Effects
  use Cizen.Effectful
  alias Cizen.{Event, Filter}

  alias Oxpt.TimeDiscountRate.Locales

  alias Oxpt.TimeDiscountRate.Events.{
    FetchState,
    UpdateStateAll,
    ChangePage,
    JoinGuest,
    ChangeSetting,
    UpdateGuest,
    UpdateHost
  }

  use Oxpt.Game,
    root_dir: Path.join(__ENV__.file, "../../host") |> Path.expand()

  @impl Oxpt.Game
  def player_socket(game_id, %__MODULE__{guest_game_id: guest_game_id}, guest_id) do
    %__MODULE__.PlayerSocket{
      game_id: game_id,
      guest_id: guest_id,
      guest_game_id: guest_game_id
    }
  end

  @impl Oxpt.Game
  def new(room_id, params) do
    %__MODULE__{room_id: room_id, guest_game_id: params[:game_id]}
  end

  @impl true
  def spawn(id, %__MODULE__{guest_game_id: guest_game_id}) do
    perform(id, %Subscribe{
      event_filter:
        Filter.new(fn
          %Event{
            body: %FetchState{game_id: ^id}
          } ->
            true

          %Event{
            body: %ChangePage{game_id: ^id}
          } ->
            true

          %Event{
            body: %JoinGuest{game_id: ^id}
          } ->
            true

          %Event{
            body: %ChangeSetting{game_id: ^id}
          } ->
            true

          %Event{
            body: %UpdateHost{game_id: ^id}
          } ->
            true

        end)
    })

    initial_state = %{
      guest_game_id: guest_game_id,
      players: %{},
      groups: %{},
      locales: Locales.get(),
      page: "instruction",
      matched: false,
      game_rounds: 1, #1 for default
      game_redo: 0, #0 for default
      inf_redo: false,
      initial_amount: 1000,
      time_discount_rate_results: %{}
    }

    {:loop, initial_state}
  end

  @impl true
  def yield(id, {:loop, state}) do
    event = perform(id, %Receive{})
    state = handle_event_body(id, event.body, state)

    {:loop, state}
  end

  defp reset(state) do
    new_state = match_groups(state, 2)

    new_state |> Map.merge(%{
      matched: true
    })
  end

  defp update_locales(locales_temp, state) do
    Enum.reduce(Map.keys(locales_temp), state, fn key, acc_state ->
      translations = get_in(acc_state, [:locales, String.to_existing_atom(key), :translations])
      lang_temp = locales_temp[key]

      translations = Enum.reduce(Map.keys(lang_temp), translations, fn trans_key, acc_trans ->
        put_in(acc_trans, [String.to_existing_atom(trans_key)], lang_temp[trans_key])
      end)

      put_in(acc_state, [:locales, String.to_existing_atom(key), :translations], translations)
    end)
  end

  defp handle_event_body(id, %FetchState{guest_id: _guest_id}, state) do
    new_state = if !state.matched do
      reset(state)
    else
      state
    end

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state:
          new_state |> Map.merge(%{
            locales: state.locales
          })
      }
    })

    perform(id, %Dispatch{
      body: %UpdateGuest{
        game_id: state.guest_game_id,
        state: new_state
      }
    })

    new_state
  end

  defp handle_event_body(id, %ChangePage{page: page}, state) do
    if page == "experiment" do
      IO.inspect state
    end

    new_state =
      state
      |> Map.put(:page, page)

    new_state = if page == "instruction" do
      reset(new_state)
    else
      new_state
    end

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          players: new_state.players,
          groups: new_state.groups,
          matched: new_state.matched,
          page: new_state.page
        }
      }
    })

    perform(id, %Dispatch{
      body: %UpdateGuest{
        game_id: state.guest_game_id,
        state: new_state
      }
    })

    new_state
  end

  defp handle_event_body(_id, %JoinGuest{payload: payload}, state) do
    new_state = state |> put_in([:players, payload.guest_id], payload.player)

    new_state
  end

  defp handle_event_body(id, %ChangeSetting{payload: payload}, state) do
    new_state = state |> Map.merge(%{
      game_rounds: payload["game_rounds"],
      game_redo: payload["game_redo"],
      inf_redo: payload["inf_redo"],
      initial_amount: payload["initial_amount"]
    })

    new_state = update_locales(payload["locales_temp"], new_state)

    new_state = reset(new_state)

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: new_state
      }
    })

    perform(id, %Dispatch{
      body: %UpdateGuest{
        game_id: state.guest_game_id,
        state: new_state
      }
    })

    new_state
  end

  defp handle_event_body(id, %UpdateHost{state: update_state}, state) do
    new_state = state |> Map.merge(update_state)

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: update_state
      }
    })

    new_state
  end

  defp match_groups(state, group_size) do
    initial_amount = state.initial_amount
    %{players: players} = state

    groups = players
              |> Enum.map(&elem(&1, 0)) # [id, id, ...]
              |> Enum.shuffle
              |> Enum.map_reduce(0, fn(p, acc) -> {{acc, p}, acc + 1} end) |> elem(0) # [{0, id}, {1, id}, ...]
              |> Enum.group_by(fn {i, _} -> Integer.to_string(div(i, group_size)) end, fn {_, p} -> p end)

    updater = fn player, group_id, role ->
      player |> Map.merge(%{
        role: role,
        group_id: group_id,
        point: 0
      })
    end

    reducer = fn {group_id, ids}, {players, groups} ->
      if length(ids) == 2 do
        [id1, id2]= ids
        players = players
                    |> Map.update!(id1, &updater.(&1, group_id, "proposer"))
                    |> Map.update!(id2, &updater.(&1, group_id, "responder"))
        groups = groups |> Map.put(group_id, new_group(ids, initial_amount))
        { players, groups }
      else
        [id1] = ids
        players = players
                    |> Map.update!(id1, &updater.(&1, group_id, "visitor"))
        groups = groups |> Map.put(group_id, new_group(ids, initial_amount))
        { players, groups }
      end
    end

    {players, groups} = Enum.reduce(groups, {players, %{}}, reducer)

    state |> Map.merge(%{
      players: players,
      groups: groups
    })
  end

  def new_group(members, initial_amount) do
    %{
      members: members,
      now_round: 1,
      redo_count: 0,
      allo_temp: div(initial_amount,2),
      game_state: "allocating",
      group_results: [],
      response: ""
    }
  end

end
