defmodule Oxpt.TimeDiscountRate.Guest.PlayerSocket do
  use Cizen.Automaton
  defstruct [:game_id, :guest_id]

  use Cizen.Effects
  alias Cizen.{Filter, Event}
  alias Oxpt.Player.{Input, Output}

  alias Oxpt.TimeDiscountRate.Events.{
    UpdateState,
    UpdateStateAll,
    FetchState,
    ChangeAlloTemp,
    FinishAllocating,
    Response,
    ResetResponse,
    RedoAllocating
  }

  @impl true
  def spawn(id, %__MODULE__{} = socket) do
    perform(id, %Subscribe{
      event_filter:
        Filter.new(fn
          %Event{
            body: %UpdateStateAll{game_id: ^socket.game_id}
          } ->
            true

          %Event{
            body: %UpdateState{guest_id: ^socket.guest_id}
          } ->
            true

          %Event{
            body: %Input{event: "change allo temp", guest_id: ^socket.guest_id}
          } ->
            true

          %Event{
            body: %Input{event: "finish allocating", guest_id: ^socket.guest_id}
          } ->
            true

          %Event{
            body: %Input{event: "response", guest_id: ^socket.guest_id}
          } ->
            true

          %Event{
            body: %Input{event: "redo allocating", guest_id: ^socket.guest_id}
          } ->
            true

          %Event{
            body: %Input{event: "reset response", guest_id: ^socket.guest_id}
          } ->
            true
        end)
    })

    perform(id, %Dispatch{
      body: %FetchState{
        game_id: socket.game_id,
        guest_id: socket.guest_id
      }
    })

    {:loop, socket}
  end

  @impl true
  def yield(id, {:loop, socket}) do
    event = perform(id, %Receive{})

    case event.body do
      %Input{} = input ->
        handle_input(id, input, socket)

      %UpdateStateAll{state: state} ->
        dispatch_state(id, state, socket)

      %UpdateState{state: state} ->
        dispatch_state(id, state, socket)
    end

    {:loop, socket}
  end

  defp handle_input(id, %Input{event: "change allo temp", payload: allo_temp}, socket) do
    perform(id, %Dispatch{
      body: %ChangeAlloTemp{
        game_id: socket.game_id,
        guest_id: socket.guest_id,
        allo_temp: allo_temp
      }
    })
  end

  defp handle_input(id, %Input{event: "finish allocating"}, socket) do
    perform(id, %Dispatch{
      body: %FinishAllocating{
        game_id: socket.game_id,
        guest_id: socket.guest_id
      }
    })
  end

  defp handle_input(id, %Input{event: "response", payload: accept}, socket) do
    perform(id, %Dispatch{
      body: %Response{
        game_id: socket.game_id,
        guest_id: socket.guest_id,
        accept: accept
      }
    })
  end

  defp handle_input(id, %Input{event: "reset response"}, socket) do
    perform(id, %Dispatch{
      body: %ResetResponse{
        game_id: socket.game_id,
        guest_id: socket.guest_id
      }
    })
  end

  defp handle_input(id, %Input{event: "redo allocating"}, socket) do
    perform(id, %Dispatch{
      body: %RedoAllocating{
        game_id: socket.game_id,
        guest_id: socket.guest_id
      }
    })
  end

  defp dispatch_state(id, state, socket) do
    perform(id, %Dispatch{
      body: %Output{
        game_id: socket.game_id,
        guest_id: socket.guest_id,
        event: "update_state",
        payload: %{state: state}
      }
    })
  end

end
