defmodule Oxpt.TimeDiscountRate.Locales do
  def get do
    %{
      en: %{
        translations: %{
          title: "TimeDiscountRate",
          point_unit: "points",

          #Instruction
          instruction1: "<p>You will be faced with a simple bargaining problem with only two bargainers, player 1 and player2.</p><p>In each bargaining game both players have to distribute a given amount c={{initial_amount}} amoung themthelves.</p><p>The rule of the bargaining game are as follows:</p>",
          instruction2: "<p>First player 1 can determine any amount a_1 = {{initial_amount}} between 0 and c which he clemands for himself.</p><p>The difference c - a_1 is what palyer 1 offers to player2.</p>",
          instruction3: "<p>Player2 will be informed about player 1's decision a_1.</p><p>Knowing player 1's proposal player 2 can either accept this proposal or choose conflict.</p>",
          instruction4: "<p>If player 2 accepts player 1's proposal, player 1 gets a_1, and player 2 the residual amount c - a_1.</p><p>In case of conflict both players get zero.</p>",
          instruction5: "<p>If you have any questions, we will happy to answer them now.</p><p>During the experiment it is forbidden to ask question or to make remarks.</p>",

        }
      },
      ja: %{
        translations: %{
          title: "最後通牒ゲーム",
          visitor: "見学者",
          proposer: "提案者",
          responder: "応答者",
          send: "次へ",
          close: "閉じる",
          cancel: "キャンセル",
          point_unit: "ポイント",

          #Instruction
          instruction_title: "ゲームの説明",
          instruction1: "<p>あなたはプレイヤー1とプレイヤー2の2人だけで行う簡単な交渉ゲームに直面しています。</p><p>交渉ゲームでは、2人のプレーヤーの間でc={{initial_amount}}ポイントを分配しなければなりません。</p><p>交渉ゲームのルールは以下の通りです。</p>",
          instruction2: "<p>はじめに、プレイヤー1は自分のポイントにしたい値a_1を0からcまでの範囲で決定します。</p><p>残額のc - a_1はプレイヤー1がプレイヤー2に対して提示する提案額です。</p>",
          instruction3: "<p>プレイヤー2はプレイヤー1の決定であるa_1を知らされます。</p><p>プレイヤー1の提案を知らされたプレイヤー2は、提案に同意することも拒否することもできます。</p>",
          instruction4: "<p>もしプレイヤー2がプレイヤー1の提案に同意するなら、プレイヤー1はa_1を獲得し、プレイヤー2は残額のc - a_1を獲得します。</p><p>拒否した場合は2人とも何も獲得できません。</p>",
          instruction5: "<p>質問がある場合は、いま受け付けます。</p><p>実験中に質問をしたり確認をすることは禁止します。</p>",

          #Experiment
          header_item: "項目",
          header_info: "情報",
          header_round: "ラウンド",
          header_role_change: "役割交代",
          header_suggestion: "再提案",
          header_point: "ポイント",
          round_view: "現在{{now_round}}回目 / 全{{game_rounds}}回",
          final_round: "これが最後のラウンド",
          role_change_view: "残り{{round}}回",
          suggestion_view: "現在{{redo_count}}回目 / 全{{game_redo}}回",
          inf_redo: "∞回",
          final_suggestion: "これが最後の提案",
          judging_notice: "提案者から提案されました。回答を選択してください。",
          response_OK: "提案が受理されました。",
          response_NG: "提案が拒否されました。",
          response_REDO: "提案が拒否されました。再度提案を行ってください。",
          cant_join: "参加できませんでした。ゲームの終了をお待ち下さい。",

          #Allocating
          user_role: "あなたは{{role}}です。",
          user_allocate: "スライドバーを動かして、配分額を決定してください",
          enemy_allocate: "{{enemy}}が配分額を決定するまで、しばらくお待ちください。",
          allocating: "あなたへの配分: {{user_allo}}　{{enemy}}への配分: {{enemy_allo}}",
          user_allo: "あなたへの配分: {{user_allo}}",
          enemy_allo: "{{enemy}}への配分: {{enemy_allo}}",
          user_allo_sm: "あなた: {{user_allo}}",
          enemy_allo_sm: "受け手: {{enemy_allo}}",
          allocate_confirm: "初期保有額{{initial_amount}}ポイントを、あなたへ{{user_allo}}ポイント、相手に{{enemy_allo}}ポイントで配分するように提案します。よろしいですか。",
          propose: "提案する",

          #Judging
          send_wait: "提案しました。応答者の回答をお待ちください。",
          suggestion: "提案者が上記のように提案してきました。同意か拒否のいずれかを回答してください。",
          accept: "同意する",
          reject: "拒否する",

          #Finished
          user_role_mini: "{{role}}",
          wait_end: "終了待機",
          finished: "あなたのゲームは終了しました。ほかのペアのゲームが終了するまでお待ちください。",

          #Result
          result_graph: "グラフ",
          round_result: "各ラウンドの結果",

          #RoundResult
          result_round: "{{round}}ラウンド目",
          remain_point: "手もとに残るポイント",
          pass_point: "相手に渡すポイント",
          enemy_accepted: "相手は承認しました。",
          enemy_rejected: "相手は拒否しました。",
          return_point: "相手から受け取るポイント",
          user_accepted: "あなたは承認しました。",
          user_rejected: "あなたは拒否しました。",

          #Chart
          allo_point: "提案者に分配されたポイント",
          chart_count: "回数",
          result_accept: "承認",
          result_reject: "拒否",
          chart_round: "表示ラウンド: {{chart_round}}",
          all_round: "全ラウンド合算"
        }
      }
    }
  end
end
